# Hikers Hell

Welcome to Hikers Hell, a first-person survival voxel game with an infinite and randomly generated trail. In this game, you must explore the cursed trail, manage your hunger, thirst, and fatigue, and survive in a harsh environment. As you progress, you will uncover the story behind the cursed trail and face challenging obstacles that test your survival skills.
# Installation

To play Hikers Hell, you will need to purchase the game and download the game files from itch.io. Once you have downloaded the files, follow these steps:

    Extract the files from the downloaded archive to a folder on your computer.
    Run the executable file to start the game.

# Gameplay

In Hikers Hell, you are a hiker who has stumbled upon a cursed trail that is infinite and randomly generated. As you explore the trail, you must manage your hunger, thirst, and fatigue by collecting resources, hunting animals, and finding shelter. You will encounter challenging obstacles, such as dangerous creatures, harsh weather conditions, and treacherous terrain.

As you progress, you will uncover the story behind the cursed trail and discover hidden secrets that are scattered throughout the environment. Your ultimate goal is to survive as long as possible and uncover the mysteries of the trail.
Controls

The controls for Hikers Hell are as follows:

    Move: W, A, S, D
    Jump: Spacebar
    Crouch: Left Ctrl
    Sprint: Left Shift
    Interact: E
    Attack: Left Mouse Button
    Inventory: Tab

# Contributing

Hikers Hell is an open-source game, and we welcome contributions from the community. If you would like to contribute to the game, please read our contributing guidelines and submit a pull request.
License

Hikers Hell is licensed under the MIT License. You are free to use, modify, and distribute the game, as long as you credit the original authors and include the license file.
